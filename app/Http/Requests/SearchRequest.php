<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'departure' => 'string',
            'country' => 'string',
            'datefrom' => 'string',
            'dateto' => 'string',
            'nightsfrom' => 'string',
            'nightsto' => 'string',
            'adults' => 'string',
            'child' => 'string',
            'childage1' => 'string',
            'childage2' => 'string',
            'childage3' => 'string',
            'stars' => 'string',
            'starsbetter' => 'string',
            'meal' => 'string',
            'mealbetter' => 'string',
            'rating' => 'string',
            'hotels' => 'string',
            'hoteltypes' => 'string',
            'pricetype' => 'string',
            'regions' => 'string',
            'subregions' => 'string',
            'operators' => 'string',
            'pricefrom' => 'string',
            'priceto' => 'string',
            'currency' => 'string',
            'hideregular' => 'string'
        ];
    }
}
