<?php

namespace App\Http\Controllers;

use App\Http\Requests\CountriesRequest;
use App\Http\Requests\FlyDatesRequest;
use App\Http\Requests\HotelsRequest;
use App\Http\Requests\OperatorsRequest;
use App\Http\Requests\RegionsRequest;
use App\Http\Requests\ResultSearchRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\SubRegionsRequest;
use App\Services\Tourvisor\TourVisorService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ToursController extends Controller
{
    protected $tourVisor;

    public function __construct()
    {
        $this->tourVisor = new TourVisorService();
    }

    /**
     * List of cities departures
     * @return \Illuminate\Http\Client\Response
     */
    public function departures()
    {
        return $this->tourVisor->getDepartures();
    }

    /**
     * List of countries
     * @param CountriesRequest $request
     * @return \Illuminate\Http\Client\Response
     */
    public function countries(CountriesRequest $request)
    {
        return $this->tourVisor->getCountries($request->all());
    }

    /**
     * List of regions
     * @param RegionsRequest $request
     * @return \Illuminate\Http\Client\Response
     */
    public function regions(RegionsRequest $request)
    {
        return $this->tourVisor->getRegions($request->all());
    }

    /**
     * List of sub regions
     * @param SubRegionsRequest $request
     * @return \Illuminate\Http\Client\Response
     */
    public function subRegions(SubRegionsRequest $request)
    {
        return $this->tourVisor->getSubRegions($request->all());
    }

    /**
     * Get list meals
     * @return \Illuminate\Http\Client\Response
     */
    public function meals()
    {
        return $this->tourVisor->getMeals();
    }

    /**
     * Get list stars of hotel
     * @return \Illuminate\Http\Client\Response
     */
    public function stars()
    {
        return $this->tourVisor->getStars();
    }

    /**
     * Get list hotels
     * @param HotelsRequest $request
     * @return \Illuminate\Http\Client\Response
     */
    public function hotels(HotelsRequest $request)
    {
        return $this->tourVisor->getHotels($request->all());
    }

    /**
     * Get list of operators
     * @param OperatorsRequest $request
     * @return \Illuminate\Http\Client\Response
     */
    public function operators(OperatorsRequest $request)
    {
        return $this->tourVisor->getOperators($request->all());
    }

    /**
     * Get fly dates
     * @param FlyDatesRequest $request
     * @return \Illuminate\Http\Client\Response
     */
    public function flyDates(FlyDatesRequest $request)
    {
        return $this->tourVisor->getFlyDates($request->all());
    }

    /**
     * Get currencies bank course
     * @return \Illuminate\Http\Client\Response
     */
    public function currency()
    {
        return $this->tourVisor->getCurrency();
    }

    /**
     * Search tour of the tourvisor
     * @param SearchRequest $request
     * @return \Illuminate\Http\Client\Response
     */
    public function search(SearchRequest $request)
    {
        return $this->tourVisor->search($request->all());
    }

    public function resultSearch(ResultSearchRequest $request)
    {
        return $this->tourVisor->searchResult($request->all());
    }


}
