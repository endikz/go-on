<?php


namespace App\Services\Tourvisor;


use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class TourVisorService
{
    public function __construct()
    {
        $this->tv = config('services.tourvisor');
    }

    /**
     * @return \Illuminate\Http\Client\Response
     */
    public function getDepartures()
    {
        return $this->request($this->tv['url'], ['type' => 'departure']);
    }

    /**
     * @param array $params
     * @return \Illuminate\Http\Client\Response
     */
    public function getCountries(array $params)
    {
        $params['type'] = 'country';
        return $this->request($this->tv['url'], $params);
    }

    /**
     * @param array $params
     * @return \Illuminate\Http\Client\Response
     */
    public function getRegions(array $params)
    {
        $params['type'] = 'region';
        return $this->request($this->tv['url'], $params);
    }

    /**
     * @param array $params
     * @return \Illuminate\Http\Client\Response
     */
    public function getSubRegions(array $params)
    {
        $params['type'] = 'subregion';
        return $this->request($this->tv['url'], $params);
    }

    /**
     * @return \Illuminate\Http\Client\Response
     */
    public function getMeals()
    {
        return $this->request($this->tv['url'], ['type' => 'meal']);
    }

    /**
     * @return \Illuminate\Http\Client\Response
     */
    public function getStars()
    {
        return $this->request($this->tv['url'], ['type' => 'stars']);
    }

    /**
     * @param array $params
     * @return \Illuminate\Http\Client\Response
     */
    public function getHotels(array $params)
    {
        $params['type'] = 'hotel';
        return $this->request($this->tv['url'], $params);
    }

    /**
     * @param array $params
     * @return \Illuminate\Http\Client\Response
     */
    public function getOperators(array $params)
    {
        $params['type'] = 'operator';
        return $this->request($this->tv['url'], $params);
    }

    /**
     * @param array $params
     * @return \Illuminate\Http\Client\Response
     */
    public function getFlyDates(array $params)
    {
        $params['type'] = 'flydate';
        return $this->request($this->tv['url'], $params);
    }

    /**
     * @return \Illuminate\Http\Client\Response
     */
    public function getCurrency()
    {
        return $this->request($this->tv['url'], ['type' => 'currency']);
    }

    /**
     * @param array $params
     * @return \Illuminate\Http\Client\Response
     */
    public function search(array $params)
    {
        $params['format'] = 'json';
        return $this->request($this->tv['search_url'], $params);
    }

    /**
     * @param array $params
     * @return \Illuminate\Http\Client\Response
     */
    public function searchResult(array $params)
    {
        $params['format'] = 'json';
        return $this->request($this->tv['result_url'], $params);
    }

    /**
     * @param string $url
     * @param array $params
     * @return \Illuminate\Http\Client\Response
     */
    private function request(string $url, array $params)
    {
        $params['authlogin'] = $this->tv['login'];
        $params['authpass'] = $this->tv['password'];

        Log::info($url . "?" . http_build_query($params));
        return Http::get($url . "?" . http_build_query($params));
    }
}
