<?php

namespace Tests\Unit;


use Tests\TestCase;

class ToursTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_departures()
    {
        $response = $this->getJson('/api/departures');

        $response
            //  ->dump()
            ->assertStatus(200);
    }

    public function test_countries()
    {
        $response = $this->getJson('/api/countries?cndep=74');
        $response
            //     ->dump()
            ->assertStatus(200);
    }

    public function test_regions()
    {
        $response = $this->getJson('/api/regions');
        $response
            //   ->dump()
            ->assertStatus(200);
    }

    public function test_sub_regions()
    {
        $response = $this->getJson('/api/sub-regions');
        $response
            // ->dump()
            ->assertStatus(200);
    }

    public function test_meal()
    {
        $response = $this->getJson('/api/meals');
        $response
            // ->dump()
            ->assertStatus(200);
    }

    public function test_stars()
    {
        $response = $this->getJson('/api/stars');
        $response
            //->dump()
            ->assertStatus(200);
    }

    public function test_hotels()
    {
        $response = $this->getJson('/api/hotels?hotcountry=1');
        $response
         //  ->dump()
            ->assertStatus(200);
    }

    public function test_operators()
    {
        $response = $this->getJson('/api/operators');
        $response
            //  ->dump()
            ->assertStatus(200);
    }

    public function test_fly_dates()
    {
        $response = $this->getJson('/api/fly-dates?flydeparture=3&flycountry=1');
        $response
             // ->dump()
            ->assertStatus(200);
    }

    public function test_currency()
    {
        $response = $this->getJson('/api/currency');
        $response
           //  ->dump()
            ->assertStatus(200);
    }

    public function test_search()
    {
        $response = $this->getJson('/api/search');
        $response
             // ->dump()
            ->assertStatus(200);
    }

    public function test_result()
    {
        $response = $this->getJson('/api/search');
        $data = json_decode($response->content(), true);
        $reqId = $data['result']['requestid'] ?? null;

        if(!is_null($reqId))
        {
            sleep(10);
            $res = $this->getJson('/api/result?requestid='.$reqId);
            $res
                ->dump()
                ->assertStatus(200);
        }
    }

}
