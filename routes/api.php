<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ToursController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('departures', [ToursController::class, 'departures'])->name('departures');
Route::get('countries', [ToursController::class, 'countries'])->name('countries');
Route::get('regions', [ToursController::class, 'regions'])->name('regions');
Route::get('sub-regions', [ToursController::class, 'subRegions'])->name('sub-regions');
Route::get('meals', [ToursController::class, 'meals'])->name('meals');
Route::get('stars', [ToursController::class, 'stars'])->name('stars');
Route::get('hotels', [ToursController::class, 'hotels'])->name('hotels');
Route::get('operators', [ToursController::class, 'operators'])->name('operators');
Route::get('fly-dates', [ToursController::class, 'flyDates'])->name('fly-dates');
Route::get('currency', [ToursController::class, 'currency'])->name('currency');
Route::get('search', [ToursController::class, 'search'])->name('search');
Route::get('result', [ToursController::class, 'resultSearch'])->name('result');


//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
